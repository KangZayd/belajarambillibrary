package com.kangzayd.belajarambillibrary;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MenuUtamaActivity extends AppCompatActivity {
    private TextView textBeranda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);
        textBeranda = (TextView)findViewById(R.id.text_beranda);
        textBeranda.setText("halaman Beranda"); // set Text value melalui java
    }
}
