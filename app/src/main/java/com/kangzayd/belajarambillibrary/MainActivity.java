package com.kangzayd.belajarambillibrary;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.wang.avi.AVLoadingIndicatorView;

public class MainActivity extends AppCompatActivity {
    private AVLoadingIndicatorView loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loader = (AVLoadingIndicatorView)findViewById(R.id.avi);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               startActivity(new Intent(MainActivity.this, MenuUtamaActivity.class));
                MainActivity.this.finish();
            }
        }, 2000); // untuk menuju ke halaman selanjutnya setelah jeda waktu yg ditentukan
    }
}
